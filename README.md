2.9.15
- Klappfallscheiben auf der Standortschießanlage nun funktionstüchtig
- Neuer Kasernebereich im NW des Flugfeldes
- Mehrere Bugfixes
- Kistenfüller im Cargobereich
- Rucksack auf den Bauch schnallen nun über ACE-Interaktionsmenü verfügbar (+Position etwas justiert)
- VCOM AI eingebunden (AI sollte nun etwas besser fahren können bzw. Infanteriegruppen sich besser positionieren [Deckung besser nutzen usw.]), Feedback erwünscht
- Komplett neuer Ablauf im Hintergrund der Mission (um ein wiederverwertbares Missionstemplate zu haben)
- Erweitertes Aussenlastsystem

2.9.11
- Gebäude im Barbaradorf nicht mehr zerstörbar
- Alle Spieler sollten nun durchsuchbar sein
- Kamerascript für Dr. Thodt
- Update der Slotliste an derzeitige Verwendung

2.9.9
- ACE PBO Check eingebaut, derzeit mit permanenter Warnung (Prüft ob Addons aktuell und vollständig sind)
- Zeus - Module gesetzt (Manu, D0T, Dr.Thodt + Admin) - die Nutzung ist gleichzeitig möglich
- ACE Mapausleuchtung wieder aktiviert, also vergesst die Taschenlampe nicht.

2.9.6
- Neuer Sprungbereich westlich des Barbaradorfes (erreichbar über Flagge in der Kaserne)
- Kleinere Reste von TFAR entfernt

2.9.5
- ACRE
- TFAR entfernt
- ACE-Inventar für Fahrzeuge ist nun an

2.9.2
- Flagge zum Krasnostav AirField + Männlein das einem schöne Luftfahrzeuge dort zusammenbaut
- Komplette Überarbeitung der Schiessbahn (Repo-Update notwendig!), Klappfallscheiben kommen noch, Abschlusstest steht noch aus
- Imperator als CFR-C
- Drohnen nach Krasnostav AirField verlegt

2.9.0
- Helikopter auf der Karte nur noch: NH-90, EC-635 und UH Tiger, restliche Helikopter entfernt (Apache, UH-60)
- Änderungen ACE Medic System

2.8.8
- Crewansicht in Fahrzeugen repariert: 4 Farben: Grün = Alles gut, Gelb = Verletzt, Lila = Blutet, Rot = Ohnmächtig
- Änderung der Slotliste
- Änderung ACE - "Festnehmen"

2.8.6
Änderung der Gruppe Wolf lt. STAN Liste
Änderung der Gruppe Nashorn lt. STAN Liste