#define CBA_OFF
#include "script_component.hpp"

class Extended_PreInit_EventHandlers {
    class MAIN_ADDON {
        init = "call compile preProcessFileLineNumbers 'XEH_PreInit.sqf'";
    };
};

class Extended_GetIn_Eventhandlers {
    #include "player\XEH_getIn.hpp"
};