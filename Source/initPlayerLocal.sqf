#include "script_component.hpp"
SCRIPT(initplayerlocal);

waitUntil {!isNull player};

removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

player forceAddUniform "ARC_GER_Flecktarn_Uniform";
player addHeadgear "ARC_GER_Flecktarn_CapPatrol";

player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "ItemWatch";

player addRating 99999999;
enableSentences false;
player setVariable ["ACE_isSearchable", true];

_objBuesche = [137749,137761,137757,137753,137745,137755,137762];
{([0,0,0] nearestObject _x) setDamage 1;} forEach _objBuesche;

_handle = execVM "vllBrig31_runwayExtensions\KillBushes.sqf";

[] call EFUNC(player,addbriefing);
[] call EFUNC(player,backpack_init);
[] call EFUNC(interface,crate_addaction);
[] call EFUNC(interface,crate_draw3D);
[] call EFUNC(slingload,init);
vllbrig31_hudhandle = [] call EFUNC(player,hud_init);