// dsp_copilot.sqf 1.01
// dr.strangepete (pete@shitnami.net)
// http://forums.bistudio.com/showthread.php?166678-script-mp-Copilot-Assisted-Autorotation-Training-Controls

//Please Read Completely!

//INSTALL:	place dsp_copilot.sqf in root of mission folder,
//			add these lines to your init.sqf:
//
// call compile preprocessFileLineNumbers "dsp_copilot.sqf"; /*this line -must- stay uncommented in init.sqf*/
//
//***uncomment -one- of the following 3 lines:
//
//		call dsp_fnc_Init; 	/*applies script to all specified heli class-types when called*/
//
//		["FLAGS"] call dsp_fnc_Init;	/*applies only to vehicles with a FLAG (see note)*/
//
//		[testheli1,testheli2] call dsp_fnc_Init; /*for single, or multiple vehicles specified by Object*/
//
//
//***FLAG a vehicle by inserting this code into its INIT in the mission editor:
//
//		this setVariable ["dsp_vehicle",true];
//
//***Vehicle Respawn Module - insert this code into the EXPRESSION field:
//
//		[_this select 0] call dsp_fnc_Init; missionNamespace setVariable ["dsp_vehicle",(missionNamespace getVariable ["dsp_vehicle",false])];
//
//

dsp_debug = true; /*this is for me, if you have issues, expect me to ask if this is enabled and attach your RPT file*/

dsp_var_heliClass = ["B_Heli_Light_01_F","B_Heli_Light_01_armed_F","B_Heli_Attack_01_F","B_Heli_Transport_01_F","B_Heli_Transport_01_camo_F","O_Heli_Attack_02_F","O_Heli_Attack_02_black_F","O_Heli_Light_02_F","O_Heli_Light_02_unarmed_F","I_Heli_Transport_02_F","I_Heli_light_03_F","I_Heli_light_03_unarmed_F","CH49_Mohawk_FG","VLLBrig_EC635_Unarmed","VLLBrig_EC635_AT","VLLBrig_EC635","VLLBrig_NH90Armed","VLLBrig_NH90","RHS_UH60M_MEV2_d","RHS_UH60M_d"];
dsp_var_EC = ["VLLBrig_EC635_Unarmed","VLLBrig_EC635_AT","VLLBrig_EC635"];
	
dsp_fnc_Init = {	if(!isServer) exitWith {};
						if(isNil("_this")) then {
							//no arguments - execute for all helicopters
							{
								if((typeOf (vehicle _x)) in dsp_var_heliClass) then {
									if(dsp_debug) then {diag_log format ["spawned dsp_fnc_copilotLoop for:\n%1",_x];};
									_nil = [(vehicle _x)] spawn dsp_fnc_copilotLoop;
								};
							} forEach vehicles;
						} else { //arguments! 
							if(typeName (_this select 0) == "OBJECT") then {
								{
									if(dsp_debug) then {diag_log format ["spawned dsp_fnc_copilotLoop for:\n%1",_x];};
									_nil = [_x] spawn dsp_fnc_copilotLoop;
								} forEach _this; 
							} else {
								if(typeName (_this select 0) == "STRING") then {
									if(_this select 0 == "FLAGS") then {
										{
											if ((vehicle _x) getVariable ["dsp_vehicle",false]) then {
												_nil = [(vehicle _x)] spawn dsp_fnc_copilotLoop;
											};
										} forEach vehicles;
									};
								};
							};						
						};
				};

dsp_fnc_copilotLoop =   {
							if(!isServer) exitWith {};
							_veh = _this select 0;
							//if(dsp_debug) then {diag_log "dsp_fnc_copilotLoop for " + str(_this);};
							_veh enableCopilot true;
							while {_veh == _veh} do {
								if((typeOf (_veh)) in dsp_var_EC) then {
									waitUntil {!isNull (_veh turretUnit [2])}; //wait for a copilot
									_unit = _veh turretUnit [2];
									if(dsp_debug) then {diag_log format ["...calling EC-635 dsp_fnc_localInit @ %1 for %2",_unit,_veh];};
									[[_veh],"dsp_fnc_localInit",_unit,false] call bis_fnc_mp;
									waitUntil {isNull (_veh turretUnit [2])}; //when seat is empty
								} else {
									waitUntil {!isNull (_veh turretUnit [0])}; //wait for a copilot
									_unit = _veh turretUnit [0];
									if(dsp_debug) then {diag_log format ["...calling dsp_fnc_localInit @ %1 for %2",_unit,_veh];};
									[[_veh],"dsp_fnc_localInit",_unit,false] call bis_fnc_mp;
									waitUntil {isNull (_veh turretUnit [0])}; //when seat is empty
								};
							};
						};

dsp_fnc_localInit = { 
						//add actions are local, so this is called by bis_fnc_mp from the server-side.
						// and because its only run on the copilots machine, pilot can't see these controls
						if(dsp_debug) then {diag_log "dsp_fnc_localInit started...";};
						_veh = _this select 0;
						_veh enableCopilot true;
						_nil = _veh addAction ["|<t color='#ffa900'>Engine Failure</t>",{[[_this select 0],"dsp_fnc_failEngine",(_this select 0),false] call bis_fnc_mp;},"",4.22,false];
						_nil = _veh addAction ["|<t color='#ffa900'>Tail Rotor Failure</t>",{[[_this select 0],"dsp_fnc_failTail",(_this select 0),false] call bis_fnc_mp;},"",4.21,false];
						_nil = _veh addAction ["|<t color='#55ff55'>Repair</t>",{[[_this select 0],"dsp_fnc_repairHeli",(_this select 0),false] call bis_fnc_mp;},"",4.23,false];
						_nil = _veh addAction ["|<t color='#cc1111'>Cheater</t>",{[[_this select 0],"dsp_fnc_Cheater",(_this select 0),false] call bis_fnc_mp;},"",4.24,false];
						if((typeOf (_veh)) in dsp_var_EC) then {
							waitUntil {isNull (_veh turretUnit [2])};
						}else {
							waitUntil {isNull (_veh turretUnit [0])};
						};
						call compile "removeAllActions _veh";
						if(dsp_debug) then {diag_log "dsp_fnc_localInit completed...";};
					};
					
dsp_fnc_failEngine = 	{
							_veh = _this select 0;
							if(dsp_debug) then {diag_log "dsp_fnc_failEngine called for " + str(_veh);};
							_configEngineName = getText (configfile >> "CfgVehicles" >> typeOf _veh >> "HitPoints" >> "HitEngine" >> "name");
							_veh setHit [_configEngineName,1];
						};
					
dsp_fnc_failTail =  {
						_veh = _this select 0;
						if(dsp_debug) then {diag_log "dsp_fnc_failTail called for " + str(_veh);};
						_configTailName = getText (configfile >> "CfgVehicles" >> typeOf _veh >> "HitPoints" >> "HitVRotor" >> "name");
						_veh setHit [_configTailName,1];
					};
					
dsp_fnc_Cheater =	{
						_veh = _this select 0;
						if(dsp_debug) then {diag_log "dsp_fnc_Cheater called for " + str(_veh);};
						_veh setVelocity [0,0,0];
						_veh setVectorDir (vectorDir _veh);
						_veh setVectorUp [0,0,1];
					};
					
dsp_fnc_repairHeli = 	{
							_veh = _this select 0;
							if(dsp_debug) then {diag_log "dsp_fnc_repairHeli called for " + str(_veh);};
							_veh setDamage 0;
						};

diag_log "dsp_copilot compiled";