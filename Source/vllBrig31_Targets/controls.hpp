class RscText
{
	access = 0;
	type = 0;
	idc = -1;
	colorBackground[] = 
	{
		0,
		0,
		0,
		0
	};
	colorText[] = 
	{
		1,
		1,
		1,
		1
	};
	text = "";
	fixedWidth = 0;
	x = 0;
	y = 0;
	h = 0.037;
	w = 0.3;
	style = 0;
	shadow = 1;
	colorShadow[] = 
	{
		0,
		0,
		0,
		0.5
	};
	font = "PuristaMedium";
	SizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	linespacing = 1;
};

class RscCombo
{
	access = 0;
	type = 4;
	colorSelect[] = 
	{
		0,
		0,
		0,
		1
	};
	colorText[] = 
	{
		0.95,
		0.95,
		0.95,
		1
	};
	colorBackground[] = 
	{
		0,
		0,
		0,
		1
	};
	colorScrollbar[] = 
	{
		1,
		0,
		0,
		1
	};
	soundSelect[] = 
	{
		"",
		0.1,
		1
	};
	soundExpand[] = 
	{
		"",
		0.1,
		1
	};
	soundCollapse[] = 
	{
		"",
		0.1,
		1
	};
	maxHistoryDelay = 1;
	class ScrollBar
	{
		color[] = 
		{
			1,
			1,
			1,
			0.6
		};
		colorActive[] = 
		{
			1,
			1,
			1,
			1
		};
		colorDisabled[] = 
		{
			1,
			1,
			1,
			0.3
		};
		shadow = 0;
		thumb = "\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa";
		arrowFull = "\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa";
		arrowEmpty = "\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa";
		border = "\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa";
	};
	style = 16;
	x = 0;
	y = 0;
	w = 0.12;
	h = 0.035;
	shadow = 0;
	colorSelectBackground[] = 
	{
		1,
		1,
		1,
		0.7
	};
	arrowEmpty = "\A3\ui_f\data\GUI\RscCommon\rsccombo\arrow_combo_ca.paa";
	arrowFull = "\A3\ui_f\data\GUI\RscCommon\rsccombo\arrow_combo_active_ca.paa";
	wholeHeight = 0.45;
	color[] = 
	{
		1,
		1,
		1,
		1
	};
	colorActive[] = 
	{
		1,
		0,
		0,
		1
	};
	colorDisabled[] = 
	{
		1,
		1,
		1,
		0.25
	};
	font = "PuristaMedium";
	sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
};

class RscPicture
{
	access = 0;
	type = 0;
	idc = -1;
	style = 48;
	colorBackground[] = 
	{
		0,
		0,
		0,
		0
	};
	colorText[] = 
	{
		1,
		1,
		1,
		1
	};
	font = "TahomaB";
	sizeEx = 0;
	lineSpacing = 0;
	text = "";
	fixedWidth = 0;
	shadow = 0;
	x = 0;
	y = 0;
	w = 0.2;
	h = 0.15;
};

class RscButton
{
	deletable = 0;
	fade = 0;
	access = 0;
	type = 1;
	text = "";
	colorText[] = 
	{
		1,
		1,
		1,
		1
	};
	colorDisabled[] = 
	{
		1,
		1,
		1,
		0.25
	};
	colorBackground[] = 
	{
		0,
		0,
		0,
		0.5
	};
	colorBackgroundDisabled[] = 
	{
		0,
		0,
		0,
		0.5
	};
	colorBackgroundActive[] = 
	{
		0,
		0,
		0,
		1
	};
	colorFocused[] = 
	{
		0,
		0,
		0,
		1
	};
	colorShadow[] = 
	{
		0,
		0,
		0,
		0
	};
	colorBorder[] = 
	{
		0,
		0,
		0,
		1
	};
	soundEnter[] = 
	{
		"\A3\ui_f\data\sound\RscButton\soundEnter",
		0.09,
		1
	};
	soundPush[] = 
	{
		"\A3\ui_f\data\sound\RscButton\soundPush",
		0.09,
		1
	};
	soundClick[] = 
	{
		"\A3\ui_f\data\sound\RscButton\soundClick",
		0.09,
		1
	};
	soundEscape[] = 
	{
		"\A3\ui_f\data\sound\RscButton\soundEscape",
		0.09,
		1
	};
	style = 2;
	x = 0;
	y = 0;
	w = 0.095589;
	h = 0.039216;
	shadow = 2;
	font = "RobotoCondensed";
	sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	offsetX = 0;
	offsetY = 0;
	offsetPressedX = 0;
	offsetPressedY = 0;
	borderSize = 0;
};


class VllBrig31_Targets_DLG {
	idd = 668001;
    movingEnable = true;
	controlsBackground[] = {
		"Background","Header_Frame","Header_ComboBox_Targets","Picture_ComboBox_Targets"
	};
    objects[] = {};
    controls[] = {
		"ComboBox_Targets","SaveButton","CloseButton","PopUpButton","PopDownButton","PopUpAutoButton"
	};
	
	class Background
	{
		Moving = 1;
		type = 0;
		style = 128;
		idc = -1;
		text = ""; 
		x = 0.0617188 * safezoneW + safezoneX;
		y = 0.291 * safezoneH + safezoneY;
		w = 0.314531 * safezoneW;
		h = 0.484 * safezoneH;
		colorText[] = 
		{
			0,
			0,
			0,
			0
		};
		font = "PuristaMedium";
		sizeEx = 0;
		shadow = 0;
		colorbackground[] = 
		{
			"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])",
			"(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])",
			"(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",
			"(profilenamespace getvariable ['IGUI_BCG_RGB_A',0.8])"
		};
	};
	
	class Header_Frame: RscText
	{
		idc = -1;
		text = "Einstellungen Bahn"; //--- ToDo: Localize;
		x = 0.066875 * safezoneW + safezoneX;
		y = 0.291 * safezoneH + safezoneY;
		w = 0.201094 * safezoneW;
		h = 0.033 * safezoneH;
		//sizeEx = 1.4 * GUI_GRID_H;
	};
	class Header_ComboBox_Targets: RscText
	{
		idc = -1;
		text = "Auswahl Zielscheibe"; //--- ToDo: Localize;
		x = 0.074427 * safezoneW + safezoneX;
		y = 0.336037 * safezoneH + safezoneY;
		w = 0.124271 * safezoneW;
		h = 0.0257037 * safezoneH;
	};

	class ComboBox_Targets : RscCombo
	{	
		idc = 1;
		x = 0.176405 * safezoneW + safezoneX;
		y = 0.337889 * safezoneH + safezoneY;
		w = 0.139219 * safezoneW;
		h = 0.022 * safezoneH;
		
		class ComboScrollBar
		{
			width = 0; // width of ComboScrollBar
			height = 0; // height of ComboScrollBar
			scrollSpeed = 0.01; // scrollSpeed of ComboScrollBar

			arrowEmpty = "\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa"; // Arrow
			arrowFull = "\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa"; // Arrow when clicked on
			border = "\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa"; // Slider background (stretched vertically)
			thumb = "\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa"; // Dragging element (stretched vertically)

			color[] = {1,1,1,1}; // Scrollbar color
		};
	};
	class Picture_ComboBox_Targets: RscPicture
	{
		idc = 2;
		text = "#(argb,8,8,3)color(1,1,0,1)";
		x = 0.0844271 * safezoneW + safezoneX;
		y = 0.369407 * safezoneH + safezoneY;
		w = 0.232031 * safezoneW;
		h = 0.264 * safezoneH;
	};
	class SaveButton: RscButton
	{
		idc = 20;
		text = "Übernehmen"; //--- ToDo: Localize;
		x = 0.22948 * safezoneW + safezoneX;
		y = 0.746963 * safezoneH + safezoneY;
		w = 0.068875 * safezoneW;
		h = 0.022 * safezoneH;
		textureNoShortcut = "#(argb,8,8,3)color(0,0,0,0)";
		colorFocused[] = { 1, 1, 1, 1 };
		colorBackgroundFocused[] = { 1, 1, 1, 0 };
		action = "execVM 'vllBrig31_Targets\functions\fn_SaveTexture.sqf'";
	};
	class CloseButton: RscButton
	{
		idc = 21;
		text = "Schliessen"; //--- ToDo: Localize;
		x = 0.304531 * safezoneW + safezoneX;
		y = 0.747444 * safezoneH + safezoneY;
		w = 0.0670312 * safezoneW;
		h = 0.022 * safezoneH;
		colorFocused[] = { 1, 1, 1, 1 };
		colorBackgroundFocused[] = { 1, 1, 1, 0 };		
		textureNoShortcut = "#(argb,8,8,3)color(0,0,0,0)";
		action = "closeDialog 0;";
	};
	
	class PopUpButton: RscButton
	{
		idc = 30;
		text = "Ziele hochklappen"; //--- ToDo: Localize;
		x = 0.0844271 * safezoneW + safezoneX;
		y = 0.639963 * safezoneH + safezoneY;
		w = 0.088875 * safezoneW;
		h = 0.022 * safezoneH;
		textureNoShortcut = "#(argb,8,8,3)color(0,0,0,0)";
		colorFocused[] = { 1, 1, 1, 1 };
		colorBackgroundFocused[] = { 1, 1, 1, 0 };
		action = "[0] execVM 'vllBrig31_Targets\functions\fn_PopUpChange.sqf'";
	};
	
	class PopDownButton: RscButton
	{
		idc = 31;
		text = "Ziele abklappen"; //--- ToDo: Localize;
		x = 0.17948 * safezoneW + safezoneX;
		y = 0.639963 * safezoneH + safezoneY;
		w = 0.088875 * safezoneW;
		h = 0.022 * safezoneH;
		textureNoShortcut = "#(argb,8,8,3)color(0,0,0,0)";
		colorFocused[] = { 1, 1, 1, 1 };
		colorBackgroundFocused[] = { 1, 1, 1, 0 };
		action = "[1] execVM 'vllBrig31_Targets\functions\fn_PopUpChange.sqf'";
	};
	
	class PopUpAutoButton: RscButton
	{
		idc = 40;
		text = "Ziele automatisch aufklappen: AUS"; //--- ToDo: Localize;
		x = 0.0844271 * safezoneW + safezoneX;
		y = 0.676963 * safezoneH + safezoneY;
		w = 0.158875 * safezoneW;
		h = 0.022 * safezoneH;
		textureNoShortcut = "#(argb,8,8,3)color(0,0,0,0)";
		colorFocused[] = { 1, 1, 1, 1 };
		colorBackgroundFocused[] = { 1, 1, 1, 0 };
		action = "[668001,40] execVM 'vllBrig31_Targets\functions\fn_PopUpAutoChange.sqf'";
	};
	
/*
	class ResetScoreButton: RscButtonMenu
	{
		idc = 30;
		text = "Punkte zurücksetzen"; //--- ToDo: Localize;
		x = 0.0841666 * safezoneW + safezoneX;
		y = 0.661186 * safezoneH + safezoneY;
		w = 0.0979687 * safezoneW;
		h = 0.022 * safezoneH;
	};
	class PostScoreButton: RscButtonMenu
	{
		idc = 31;
		text = "Punkte anzeigen"; //--- ToDo: Localize;
		x = 0.0843753 * safezoneW + safezoneX;
		y = 0.689815 * safezoneH + safezoneY;
		w = 0.0979687 * safezoneW;
		h = 0.022 * safezoneH;
	};
*/	
};