/*
	Author: Mossarelli
	Changed by: Toma
*/

class vllBrig31_Targets
{
	tag = "vllBrig31_Targets";
	class functions
	{
		file = "vllBrig31_Targets\functions";
		requiredAddons[] = {};
		class GetRangeName
		{
			description = "";
			preInit = 1;
			recompile = 1;
		};
		class LogicInit
		{
			description = "";
		};
		class MainInit
		{
			description = "";
			preInit = 1;
			recompile = 1;
		};
		class RegisterShooter
		{
			description = "";
		};
		class ResetScore
		{
			description = "";
		};
		class ShowScore
		{
			description = "";
		};
		class SignInit
		{
			description = "";
		};
		class TargetInit 
		{
			description = "";
		};
		class PopUpInit 
		{
			description = "";
		};		
		class UnregisterShooter
		{
			description = "";
		};
		class addActionMP
		{
			description = "";
		};
		class RegisterHitEvent
		{
			description = "";
		};
		class RegisterHitEventPopUp
		{
			description = "";
		};		
		class getTexture
		{
			description = "";
		};
		class SaveTexture
		{
			description = "";
		};
		class ShowDialog
		{
			description = "";
		};
	};
};