private ["_object"];
_object = _this select 0;

if (!isServer) exitWith {};

if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: Init Logic :%1",_object];};

//Error Message.
if (isNull _object) exitWith
{
	[
		"Error: There's no Logic referred in the script. Make sure there's one Logic per firing lane."
	] call BIS_fnc_errorMsg;
};

//Init the Array for the targets. For each target synced to the logic, they will be added to this variable.
_object setVariable ["vllBrig31_Targets_LogicTargetArray",[],true];
_object setVariable ["vllBrig31_Targets_LogicPopUpArray",[],true];

//Get the objects that are synced to the logic.
_synced_objects = synchronizedObjects _object;

//Error Message.
if (count (_synced_objects) < 2) exitWith
{
	[
		"Error: Make sure to sync all signs and targets to the logic in the editor."
	] call BIS_fnc_errorMsg;
};

//Use Target's lane number to make a unique variable for the texture.
_logicname = ([( str _object),11,12] call BIS_fnc_trimString);

//Make unique variable.
_texturename = format ["vllBrig31_Targets\img\%1.jpg",_logicname];
_object setVariable ["vllBrig31_Targets_Texture", 0 ,true];

//Init all the objects.
{
	if (_x isKindOf vllBrig31_Targets_TargetObject) then
	{
		[_x,_object] call vllBrig31_Targets_fnc_TargetInit;
	};
	if (_x isKindOf vllBrig31_Targets_SignObject) then
	{
		_x setObjectTextureGlobal [0,_texturename];
		[_x,_object] call vllBrig31_Targets_fnc_SignInit;
	};
	if (_x isKindOf vllBrig31_Targets_NumberSignObject) then
	{
		_x setObjectTextureGlobal [0,_texturename];
	};
	if (_x isKindOf vllBrig31_Targets_PopUpObject) then
	{
		[_x,_object] call vllBrig31_Targets_fnc_PopUpInit;
	};
} foreach _synced_objects;

