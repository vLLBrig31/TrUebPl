//Debug variable to get information from the scripts
vllBrig31_Targets_DEBUG = false;
if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: Init Main"];};

//Assign the initial variables to player.
player setVariable ["vllBrig31_Targets_ShooterScoreInfo",[player],false];
player setVariable ["vllBrig31_Targets_PlayerLogicName","",false];
player setVariable ["vllBrig31_Targets_ShooterCamera","",false];
player setVariable ["vllBrig31_Targets_NoAutoPopUp",true,false];

vllBrig31_Targets_CurrentTexture = 0;
vllBrig31_Targets_CurrentLogic = null;

vllBrig31_Targets_TargetTexturesTexts = ["Rundscheibe","Rundscheibe - Klein","T-Scheibe","MG Scheibe","MG Scheibe Landschaft"];
vllBrig31_Targets_TargetTextures = ["vllBrig31_Targets\targets\circle.paa","vllBrig31_Targets\targets\circleSmall.paa","vllBrig31_Targets\targets\closeRange.paa","vllBrig31_Targets\targets\mgCircles.paa","vllBrig31_Targets\targets\mgTerrain.paa"];
vllBrig31_Targets_TargetTextureY = -0.2;

//These two must be two different objects.
vllBrig31_Targets_TargetTextureObject = "VLLB31_TargetTexture";
vllBrig31_Targets_TargetObject = "Sign_F"; //Type of Object for the target. Important for the init of the logic.
vllBrig31_Targets_PopUpObject = "TargetP_Inf_Acc1_F"; 

vllBrig31_Targets_SignObject = "Land_InfoStand_V2_F"; //Type of Object for the sign. Important for the init of the logic.
vllBrig31_Targets_NumberSignObject = "UserTexture1m_F"; //Type of Object for the Number Sign. Important for the init of the logic.

vllBrig31_Targets_ShowAverages = true; //Show the average score in the score message.


