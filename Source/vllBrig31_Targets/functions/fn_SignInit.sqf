private ["_object","_Logic"];
_object = _this select 0;
_Logic = _this select 1;

if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: Init Sign :%1 | Logic: %2",_object,_Logic];};

_object allowDamage false;
_object enableSimulation false;

//Get the Range name string.
[_Logic] call vllBrig31_Targets_fnc_GetRangeName;

//Error Message.
if (isNull _Logic) exitWith
{
	[
		"Error: There's no Logic referred in the script. Make sure there's one Logic per firing lane."
	] call BIS_fnc_errorMsg;
};

//Add Actions to sign.
[[_object,("<t color=""#FFFF66"">" + ("Anmelden") + "</t>"),"vllBrig31_Targets\functions\fn_RegisterShooter.sqf",_Logic],"vllBrig31_Targets_fnc_addActionMP",true,true] spawn BIS_fnc_MP;
[[_object,("<t color=""#FF0066"">" + ("Abmelden") + "</t>"),"vllBrig31_Targets\functions\fn_UnregisterShooter.sqf",_Logic],"vllBrig31_Targets_fnc_addActionMP",true,true] spawn BIS_fnc_MP;
if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: Addaction Sign :%1 | Logic: %2",_object,_Logic];};