private ["_name","_caller","_id","_params"];
_name = _this select 0;
_caller = _this select 1;
_id = _this select 2;
_params = _this select 3;

if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: Register Shooter ALL:%1",_this];};
if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: Register Shooter %1,CallFrom %2,ID %3,Params %4",_name,_caller,_id,_params];};

//Reset score.
player setVariable ["vllBrig31_Targets_ShooterScoreInfo",[player],false];

//Get the current registered lane variable.
_logicName = player GetVariable ["vllBrig31_Targets_PlayerLogicName",""];
//Make sure the player is unregistered from the first lane first.
if (( str (_logicName) == str (_params))) exitWith
{
	systemChat "Du bist hier schon angemeldet!";
};

//Make sure the player is unregistered from the first lane first.
if ( !( str (_logicName) == str (""))) exitWith
{
	systemChat format["Du bist schon auf %1 angemeldet! Erst abmelden!",(_logicName getVariable "vllBrig31_Targets_RangeNumber")];
};

_logicName = _params;
_currPlayer = _logicName GetVariable ["vllBrig31_Targets_CurrentPlayer",""];
//Make sure no other player is registered
if ( !( str (_currPlayer) == str (""))) exitWith
{
	systemChat format["%1 ist schon auf dieser Bahn angemeldet!",(_currPlayer)];
};

//Assign lane to player.
player setVariable ["vllBrig31_Targets_PlayerLogicName",_params,false];
_logicName setVariable ["vllBrig31_Targets_CurrentPlayer",(name _caller),true];

//Register HitEventhandler
_LogicTargetArray = _LogicName getVariable "vllBrig31_Targets_LogicTargetArray";
{
	if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: Register HitEventhandler %1",_x];};
	[_x] call vllBrig31_Targets_fnc_RegisterHitEvent;
	_array = _x getVariable "vllBrig31_Targets_Points";
	if ((count _array) > 0) then
	{
		{
			deleteVehicle _x;
		}forEach _array;
	};
	_x setVariable ["vllBrig31_Targets_Points",[],true];
}
forEach _LogicTargetArray;

_LogicPopUpArray = _LogicName getVariable "vllBrig31_Targets_LogicPopUpArray";
{
	_array = _x getVariable "vllBrig31_Targets_Points";
	if ((count _array) > 0) then
	{
		{
			deleteVehicle _x;
		}forEach _array;
	};
	_x setVariable ["vllBrig31_Targets_Points",[],true];
}
forEach _LogicPopUpArray;

_num = player addAction [("<t color=""#FFFF66"">" + ("Zeige Punkte") + "</t>"),vllBrig31_Targets_fnc_ShowScore,_logicName];
_array = [_num];
_num = player addAction [("<t color=""#FFAA66"">" + ("Punkte zurücksetzen") + "</t>"),vllBrig31_Targets_fnc_ResetScore,_logicName];
_array2 = [_num];
_array = _array + _array2;
_num = player addAction [("<t color=""#756FF3"">" + ("Einstellungen") + "</t>"),vllBrig31_Targets_fnc_ShowDialog,_logicName];
_array2 = [_num];
_array = _array + _array2;

player setVariable ["vllBrig31_Targets_PlayerActions",_array,false];

//Make a global message for everyone to see.
call compile format ['
[{ systemChat "%1: %2 angemeldet. Punkte zurückgesetzt."; }, "BIS_fnc_Spawn", true, false, false] call BIS_fnc_MP;
',(_params getVariable "vllBrig31_Targets_RangeNumber"),(name _caller)
];