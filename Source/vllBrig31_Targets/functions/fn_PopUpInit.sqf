private ["_object","_LogicName"];
_object = _this select 0;
_LogicName = _this select 1;

if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: Init PopUp :%1 | Logic: %2",_object,_LogicName];};

//Error Message.
if (isNull _LogicName) exitWith
{
	[
		localize "No logic exists that was passed as a parameter to the function! Make sure the logic exists and is referenced in the function call!"
	] call BIS_fnc_errorMsg;
};

_object setVariable ["vllBrig31_Targets_TargetLogicName",_LogicName,true];
_object setVariable ["vllBrig31_Targets_Points",[],true];

//Add Current target to Logic's Array variable
_LogicPopUpArray = _LogicName getVariable "vllBrig31_Targets_LogicPopUpArray";

//Make sure the target object is not in the array and then add it.
if ( ! ( str (_object) in _LogicPopUpArray) ) then
{
	_LogicPopUpArray set [(count _LogicPopUpArray),_object];
	_LogicName setVariable ["vllBrig31_Targets_LogicPopUpArray",_LogicPopUpArray,true];
};

//Settings
_object allowDamage false;
nopop = true;
_object animate["terc",1];

_object setVariable ["vllBrig31_Targets_HitEventhandler",-1,false];