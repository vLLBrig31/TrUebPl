_animate = _this select 0;

if(!isNull(vllBrig31_Targets_CurrentLogic)) then {
		
	//SetTargets
	_LogicPopUpArray = vllBrig31_Targets_CurrentLogic getVariable "vllBrig31_Targets_LogicPopUpArray";
	{
		_x animate["terc",_animate];
		
		if (_animate == 1) then {
			nopop = true;
			player setVariable ["vllBrig31_Targets_NoAutoPopUp",true,false];
			
			_array = _x getVariable "vllBrig31_Targets_Points";
			if ((count _array) > 0) then
			{
				{
					deleteVehicle _x;
				}forEach _array;
			};
			_x setVariable ["vllBrig31_Targets_Points",[],true];
			
			_numEvent = _x getVariable "vllBrig31_Targets_HitEventhandler";
			if (_numEvent > -1) then {
				_x removeEventHandler["HitPart",_numEvent];
				_x setVariable ["vllBrig31_Targets_HitEventhandler",-1,false];
			};			
		} else {
			[_x] call vllBrig31_Targets_fnc_RegisterHitEventPopUp;			
		};
	} forEach _LogicPopUpArray;
	
	//Target
	if (_animate == 1) then {
		//Register HitEventhandler
		_LogicTargetArray = vllBrig31_Targets_CurrentLogic getVariable "vllBrig31_Targets_LogicTargetArray";
		{
			[_x] call vllBrig31_Targets_fnc_RegisterHitEvent;
			_array = _x getVariable "vllBrig31_Targets_Points";
			if ((count _array) > 0) then
			{
				{
					deleteVehicle _x;
				}forEach _array;
			};
			_x setVariable ["vllBrig31_Targets_Points",[],true];
		}
		forEach _LogicTargetArray;	
	} else {
		_LogicTargetArray = vllBrig31_Targets_CurrentLogic getVariable "vllBrig31_Targets_LogicTargetArray";
		{
			_numEvent = _x getVariable "vllBrig31_Targets_HitEventhandler";
			if (_numEvent > -1) then {
				_x removeEventHandler["HitPart",_numEvent];
				_x setVariable ["vllBrig31_Targets_HitEventhandler",0,false];
			};
			
			_array = _x getVariable "vllBrig31_Targets_Points";
			if ((count _array) > 0) then
			{
				{
					deleteVehicle _x;
				}forEach _array;
			};
			_x setVariable ["vllBrig31_Targets_Points",[],true];
		}
		forEach _LogicTargetArray;
	};
};