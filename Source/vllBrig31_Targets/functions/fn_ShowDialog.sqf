private["_params"];
if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: Show Dialog ALL:%1",_this];};
_params = _this select 3;
_textureID = _params getVariable "vllBrig31_Targets_Texture";

_logicName = player GetVariable ["vllBrig31_Targets_PlayerLogicName",""];
if (( str (_logicName) == str (""))) exitWith
{
	systemChat "Du bist bei keiner Bahn angemeldet! Keine Anmeldung, nix einstellen!";
};

if ( !( str (_logicName) == str (_params))) exitWith
{
	systemChat format["Du bist nicht auf %1 angemeldet, sondern auf %2!",(_params getVariable "vllBrig31_Targets_RangeNumber"),(_logicName getVariable "vllBrig31_Targets_RangeNumber")];
};

disableSerialization;
_dialog = createDialog "VllBrig31_Targets_DLG";
if (_dialog) then {
	_display = findDisplay 668001;
	if(!isNull(_display)) then {
		//Combobox
		_ComboBox = _display displayCtrl 1;
		lbClear _ComboBox;	
		{		
			_index = _ComboBox lbAdd _x;
		} forEach vllBrig31_Targets_TargetTexturesTexts;
		
		vllBrig31_Targets_CurrentLogic = _params;
		
		_ComboBox lbSetCurSel _textureID;
		
		(_ComboBox)  ctrlAddEventHandler ["LBSelChanged","_this execVM 'vllBrig31_Targets\functions\fn_ChangeSelection.sqf'"];
		
		_PictureBox = _display displayCtrl 2;
		_PictureBox ctrlSetText (_textureID call vllBrig31_Targets_fnc_getTexture);	
				
		_PopValue = player GetVariable ["vllBrig31_Targets_NoAutoPopUp",true];

		//Textbox
		_ctrl = _display displayCtrl 40;
		if (_PopValue) then {
			_ctrl ctrlSetText "Ziele automatisch aufklappen: AUS";
		} else {
			_ctrl ctrlSetText "Ziele automatisch aufklappen: EIN";
		};
	};
};