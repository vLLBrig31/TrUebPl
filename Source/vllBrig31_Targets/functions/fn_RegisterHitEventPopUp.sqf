//Add Score counter to Target Event Handler
private["_object"];
_object = _this select 0;
if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] PopUp: HitEventhandler for %1",_object];};

_numEvent = _object getVariable "vllBrig31_Targets_HitEventhandler";
if (_numEvent > -1) then {
	_object removeEventHandler["HitPart",_numEvent];
	_object setVariable ["vllBrig31_Targets_HitEventhandler",-1,false];
};

_numberEvent = _object addEventHandler
["HitPart",
{
	_target = (_this select 0 select 0);
	_shooter = (_this select 0 select 1);
	_LogicNameTarget = _target getVariable ["vllBrig31_Targets_TargetLogicName","Null"];
	_LogicNameShooter = _shooter getVariable ["vllBrig31_Targets_PlayerLogicName","Nullify"];

	if (vllBrig31_Targets_DEBUG) then
	{
		systemChat format ["Hit: %1",_this select 0 select 3];				
	};	
	
	//Make sure the lane logic is the same object for both player and target.
	if ( str _LogicNameTarget != str _LogicNameShooter ) exitWith { };
	
	
	_pos = _target worldToModel (_this select 0 select 3);
	_posZ = ((_pos select 2) - (getPosASL _target select 2));
	_attachToPosition = [_pos select 0,0.15,_posZ];	
	
	[_target,_attachToPosition] spawn
	{
		_target = _this select 0;
		_position = _this select 1;		
		
		_spr = "vllbrig31_bullet_marker" createVehicle [0,0,0];
		_spr enableSimulation false;
		_spr attachTo[_target,_position];						
	
		if (vllBrig31_Targets_DEBUG) then
		{
			systemChat format ["AttachTo Position: %1",_position];				
		};	
	
		if (vllBrig31_Targets_DEBUG) then
		{
			systemChat format ["AttachTo Position: %1",_position];				
		};	
	
		_array = [];
		_array set [0,_spr];
		_array2 = _target getVariable "vllBrig31_Targets_Points";
		_target setVariable ["vllBrig31_Targets_Points",_array2+_array,true];					
	};
}
];


_object setVariable ["vllBrig31_Targets_HitEventhandler",_numberEvent,false];

//Typical EventHandler parameters
/*
[
	M_1, //Hit object
	"B Alpha 1-1:1 (Mossarelli)", //Shooter
	"164029: red.p3d", //Bullet
	[6383.76,5426.67,11.1529], //Bullet Impact Position
	[0.0308513,-716.366,70.7472], //Velocity In Arma XYZ
	["target"], //
	[10,0,0,0,"B_65x39_Caseless"], //Ammo
	[0.699701,0.714436,-0], //Hit Vector Direction
	0.681982, //Size of hit
	"A3\data_f\Penetration\metal_plate.bisurf", //Surface Type
	true //Direct Hit (If the bullet hit anything before hitting the target)
]
*/
