if(!isNull(vllBrig31_Targets_CurrentLogic)) then {
	vllBrig31_Targets_CurrentLogic setVariable ["vllBrig31_Targets_Texture", vllBrig31_Targets_CurrentTexture ,true];
	
	//SetTargets
	_LogicTargetArray = vllBrig31_Targets_CurrentLogic getVariable "vllBrig31_Targets_LogicTargetArray";
	{
		_textureObj = _x getVariable "vllBrig31_Targets_TargetTextureObject";
		_textureObj setObjectTextureGlobal [0,vllBrig31_Targets_CurrentTexture call vllBrig31_Targets_fnc_getTexture];
	}
	forEach _LogicTargetArray;
};

