disableSerialization;

//Get the passed ctrl
_ctrl = _this select 0;
//Get passed selected index
_index = _this select 1;

_display = ctrlParent _ctrl;
_PictureBox = _display displayCtrl 2;
_PictureBox ctrlSetText (_index call vllBrig31_Targets_fnc_getTexture);

vllBrig31_Targets_CurrentTexture = _index;