private ["_name","_caller","_id","_params"];
_name = _this select 0;
_caller = _this select 1;
_id = _this select 2;
_params = _this select 3;

//Get Distance from center array information.
_bulletinfo = player getVariable "vllBrig31_Targets_ShooterScoreInfo";
_logicName = player GetVariable ["vllBrig31_Targets_PlayerLogicName",""];

if ( ( str (_logicName) == str (""))) exitWith
{
	systemChat "Du bist bei keiner Bahn angemeldet!";
};

if ( !( str (_logicName) == str (_params))) exitWith
{
	systemChat format["Du bist nicht auf %1 angemeldet, sondern auf %2!",(_params getVariable "vllBrig31_Targets_RangeNumber"),(_logicName getVariable "vllBrig31_Targets_RangeNumber")];
};


if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: ShowScore ALL:%1",_this];};
if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: ShowScore :%1 | Logic: %2",_name,_logicName];};

_textureID = _logicName getVariable "vllBrig31_Targets_Texture"; 

//Initiate variables used in next scope.
_score = 0;
_scoreinfo = "";

if ((count _bulletinfo) == 1) exitWith { systemChat "Keine Punkte verfügbar."; };

if (_textureID == 0) then {

	/*
	The next object has been calibrated for the default target used in the script called T_1.jpg. If you for some reason do not like this target and want to use your own, you can easily edit the script below to add your own scoring.
	*/
	{
		if (_forEachIndex > 0) then //The first object in the index will always be an array with the object player. So naturally we want to skip this index.
		{		
			_bullet = _x; //This is the calibrated distance from the center of target that the bullet hit.
			//_bulletOnTarget = _x select 1; //This variable is used in case the player hits the target on the model that is not the target area. It was used for pop up targets when hitting the metal box which does not count as a hit but the game would turn the target down anyway.			
			_bulletscore = 0; //Init the variable so it can be passed from the next scope.
						
			_xPos = _bullet select 0;
			_yPos = _bullet select 2;
			_bulletFromCenter = sqrt(_xPos * _xPos + _yPos * _yPos);
			
			switch (true) do {
				case (_bulletFromCenter <= 0.05): {_bulletscore = 10};
				case (_bulletFromCenter <= 0.1): {_bulletscore = 9};
				case (_bulletFromCenter <= 0.16): {_bulletscore = 8};
				case (_bulletFromCenter <= 0.21): {_bulletscore = 7};
				case (_bulletFromCenter <= 0.28): {_bulletscore = 6};
				case (_bulletFromCenter <= 0.333): {_bulletscore = 5};
				case (_bulletFromCenter <= 0.385): {_bulletscore = 4};
				case (_bulletFromCenter <= 0.443): {_bulletscore = 3};
				case (_bulletFromCenter <= 0.5): {_bulletscore = 2};
				case (_bulletFromCenter <= 0.552): {_bulletscore = 1};
			};
						
			_score = _score + _bulletscore;
			
			if (vllBrig31_Targets_DEBUG) then
			{
				systemChat format ["Bullet %1: A:%2 | D:%3 | S:%4 |GS: %5",_forEachIndex,_bullet,_bulletFromCenter,_bulletscore,_score];
			};	
		};
	} foreach _bulletinfo;
	
	//Format the score message. Change the test variable in MainInit.
	if (vllBrig31_Targets_ShowAverages) then
	{
		//With average
		_scoreinfo = format ["%1 Punkte im Durchschnitt %2 bei %3 Schuss",_score,(_score / ((count _bulletinfo) - 1)),((count _bulletinfo) - 1)];
	} else {
		//Without average
		_scoreinfo = format ["%1 Punkte, mit %2 Schuss",_score,((count _bulletinfo) - 1)];
	};
};

if (_textureID > 0) then {
	_scoreinfo = format ["%1 Schuss abgegeben",((count _bulletinfo) - 1)];
};

//Broadcast score
call compile format
[
	'
		[ { systemChat "%1: %2 Ergebnis: %3."; }, "BIS_fnc_Spawn", true, false, false ] call BIS_fnc_MP;
	',(_params getVariable "vllBrig31_Targets_RangeNumber"),(name _caller),_scoreinfo
];