_displayID = _this select 0;
_ctrlID = _this select 1;

_PopValue = player GetVariable ["vllBrig31_Targets_NoAutoPopUp",true];
_PopValue = !_PopValue;
player setVariable ["vllBrig31_Targets_NoAutoPopUp",_PopValue,false];

disableSerialization;
_display = findDisplay _displayID;
if(!isNull(_display)) then {
	//Textbox
	_ctrl = _display displayCtrl _ctrlID;
	if (_PopValue) then {
		_ctrl ctrlSetText "Ziele automatisch aufklappen: AUS";
	} else {
		_ctrl ctrlSetText "Ziele automatisch aufklappen: EIN";
	};	
};

if(!isNull(vllBrig31_Targets_CurrentLogic)) then {
		
	//SetTargets
	_LogicPopUpArray = vllBrig31_Targets_CurrentLogic getVariable "vllBrig31_Targets_LogicPopUpArray";
	{
		nopop = _PopValue;
	}
	forEach _LogicPopUpArray;
};