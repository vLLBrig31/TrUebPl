private["_object","_title","_scriptToCall","_arguments"];
_object = _this select 0;
_title = _this select 1;
_scriptToCall = _this select 2;
_arguments = _this select 3;

if(isNull _object) exitWith {};

_object addaction [_title,_scriptToCall,_arguments,6];