private ["_name","_caller","_id","_params"];
_name = _this select 0;
_caller = _this select 1;
_id = _this select 2;
_params = _this select 3;

if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: Reset Score ALL:%1",_this];};

_logicName = player GetVariable ["vllBrig31_Targets_PlayerLogicName",""];

if ( ( str (_logicName) == str (""))) exitWith
{
	systemChat "Du bist bei keiner Bahn angemeldet!";
};

if ( !( str (_logicName) == str (_params))) exitWith
{
	systemChat format["Du bist nicht auf %1 angemeldet, sondern auf %2!",(_params getVariable "vllBrig31_Targets_RangeNumber"),(_logicName getVariable "vllBrig31_Targets_RangeNumber")];
};

//Reset score.
player setVariable ["vllBrig31_Targets_ShooterScoreInfo",[player],false];

if ( ( str (_logicName) != str (""))) then {
	_LogicTargetArray = _logicName getVariable "vllBrig31_Targets_LogicTargetArray";
	{
		_array = _x getVariable "vllBrig31_Targets_Points";
		if ((count _array) > 0) then
		{
			{
				deleteVehicle _x;
			}forEach _array;
		};
		_x setVariable ["vllBrig31_Targets_Points",[],true];
	} forEach _LogicTargetArray;
	
	_LogicPopUpArray = _logicName getVariable "vllBrig31_Targets_LogicPopUpArray";
	{
		_array = _x getVariable "vllBrig31_Targets_Points";
		if ((count _array) > 0) then
		{
			{
				deleteVehicle _x;
			}forEach _array;
		};
		_x setVariable ["vllBrig31_Targets_Points",[],true];
	}
	forEach _LogicPopUpArray;
};

//Only local message.
systemChat "Deine Punkte wurden zurückgesetzt";