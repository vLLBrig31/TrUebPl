private ["_name","_caller","_id","_params"];
_name = _this select 0;
_caller = _this select 1;
_id = _this select 2;
_params = _this select 3;

if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: Unregister Shooter ALL:%1",_this];};

_logicName = player GetVariable ["vllBrig31_Targets_PlayerLogicName",""];

//Make sure player can't unregister without being registered.
if ( ( str (_logicName) == str (""))) exitWith
{
	systemChat "Du bist bei keiner Bahn angemeldet!";
};

//Make sure player can't unregister from a lane he's not registered to.
if ( !( str (_logicName) == str (_params))) exitWith
{
	systemChat format["Du bist nicht auf %1 angemeldet, sondern auf %2!",(_params getVariable "vllBrig31_Targets_RangeNumber"),(_logicName getVariable "vllBrig31_Targets_RangeNumber")];
};

_logicName setVariable ["vllBrig31_Targets_CurrentPlayer","",true];

//Remove Eventhandler
_LogicTargetArray = _LogicName getVariable "vllBrig31_Targets_LogicTargetArray";
{
	if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: Unregister HitEventhandler %1",_x];};
	_numEvent = _x getVariable "vllBrig31_Targets_HitEventhandler";
	if (_numEvent > -1) then {
		_x removeEventHandler["HitPart",_numEvent];
		_x setVariable ["vllBrig31_Targets_HitEventhandler",-1,false];
	};
	
	_array = _x getVariable "vllBrig31_Targets_Points";
	if ((count _array) > 0) then
	{
		{
			deleteVehicle _x;
		}forEach _array;
	};
	_x setVariable ["vllBrig31_Targets_Points",[],true];
}
forEach _LogicTargetArray;

//Remove Eventhandler PopUp
_LogicPopUpArray = _LogicName getVariable "vllBrig31_Targets_LogicPopUpArray";
{
	_numEvent = _x getVariable "vllBrig31_Targets_HitEventhandler";
	if (_numEvent > -1) then {
		_x removeEventHandler["HitPart",_numEvent];
		_x setVariable ["vllBrig31_Targets_HitEventhandler",-1,false];
	};
	
	_array = _x getVariable "vllBrig31_Targets_Points";
	if ((count _array) > 0) then
	{
		{
			deleteVehicle _x;
		}forEach _array;
	};
	_x setVariable ["vllBrig31_Targets_Points",[],true];
}
forEach _LogicPopUpArray;

//remove player actions
_playerActions = player GetVariable "vllBrig31_Targets_PlayerActions";
{
	player removeAction _x;
}forEach _playerActions;



player setVariable ["vllBrig31_Targets_ShooterScoreInfo",[player],false];
player setVariable ["vllBrig31_Targets_PlayerLogicName","",false];

//Broadcast that lane is free.
call compile format ['
[{ systemChat "%1: %2 abgemeldet. Bahn ist frei."; }, "BIS_fnc_Spawn", true, false, false] call BIS_fnc_MP;
',(_params getVariable "vllBrig31_Targets_RangeNumber"),(name _caller)
];