private ["_object","_LogicName","_LogicTargetArray"];
_object = _this select 0;
_LogicName = _this select 1;

_object allowDamage false;

if (vllBrig31_Targets_DEBUG) then {diag_log format["[VLLBRIG31] Targets: Init Target :%1 | Logic: %2",_object,_LogicName];};

//Error Message.
if (isNull _LogicName) exitWith
{
	[
		localize "No logic exists that was passed as a parameter to the function! Make sure the logic exists and is referenced in the function call!"
	] call BIS_fnc_errorMsg;
};

//Make sure Target knows its logic's name
_object setVariable ["vllBrig31_Targets_TargetLogicName",_LogicName,true];
_object setVariable ["vllBrig31_Targets_Points",[],true];

//Add Current target to Logic's Array variable
_LogicTargetArray = _LogicName getVariable "vllBrig31_Targets_LogicTargetArray";

//Make sure the target object is not in the array and then add it.
if ( ! ( str (_object) in _LogicTargetArray) ) then
{
	_LogicTargetArray set [(count _LogicTargetArray),_object];
	_LogicName setVariable ["vllBrig31_Targets_LogicTargetArray",_LogicTargetArray,true];
};

//Set the texture on the object.
_texture = _LogicName getVariable "vllBrig31_Targets_Texture";
_textureObj = vllBrig31_Targets_TargetTextureObject createVehicle [0,0,0];			
_textureObj setObjectTextureGlobal [0,_texture call vllBrig31_Targets_fnc_getTexture];
_textureObj attachTo [_object,[0,vllBrig31_Targets_TargetTextureY,0.6]];
_object setVariable ["vllBrig31_Targets_TargetTextureObject",_textureObj,true];

_object setVariable ["vllBrig31_Targets_HitEventhandler",-1,false];
