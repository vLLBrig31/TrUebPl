//***********Löschen / Ausblenden IDs**************
//*************************************************
if (!isServer) exitWith {};
_t1 = diag_tickTime;

_objFlughafenLichterNordIds = [137102,137103,137104,137105,137106,137107,137108,137109,137110,137111,137112,137113];
_objFlughafenLichterSuedIds = [146087,146088,146131,146132,146133,146134,146128,146129,146130,146127,145280,146086];
_objLandebahnLichterRechtsIds = [137167,137168,137172,137169,137170,137171,137383,137382,137381,137411,137409,137410,137408,137456,137457,137458,137459,137461,137460,137455,143264,143591,143592,143593,143595,143594,143634,143638,143639,143637,143636,143635,143663,144057,144051,144052,144053,144054,144055,144056,144117,144116,144115,144176,144177,144178,144860,144855,144856,144858,144859,144857,144890,144888,144889,144935,144933,144932,144934,145221,145222,145224,145223,145220,145219];
_objLandebahnLichterLinksIds = [146091,146092,145217,145218,144930,144929,144928,144926,144927,144931,144925,144885,144886,144887,144854,144853,144852,144851,144175,144174,144173,144172,144133,144045,144049,144050,144048,144047,144046,143658,143659,143660,143657,143661,143662,143633,143589,143588,143587,143586,143590,143585,137711,137452,137453,137454,137406,137405,137404,137402,137403,137407,137401,137175,137176,137164,137165,137163,137162,137101];

_objControlTowerIds = [144153,970819];
_objHousesRightTaxiwayIds = [143923,143871,143907,143866,143872,970817];
_objTaxiwayStoerungenIds = [145201,137271,137078,137080,137079,970804,970805,137075,143784,143805,143804,143803,143802,143801,143800,143799,143798,143797,137736,145868,992962,970821,970828];
_objHangarIds = [145501,145500,145499,145396,144163,144162,144160,144161];

//Ids ausblenden
{([0,0,0] nearestObject _x) hideObjectGlobal true;([0,0,0] nearestObject _x) enableSimulationGlobal false;} forEach _objFlughafenLichterNordIds;
{([0,0,0] nearestObject _x) hideObjectGlobal true;([0,0,0] nearestObject _x) enableSimulationGlobal false;} forEach _objFlughafenLichterSuedIds;
//{([0,0,0] nearestObject _x) hideObjectGlobal true;} forEach _objLandebahnLichterRechtsIds;
//{([0,0,0] nearestObject _x) hideObjectGlobal true;} forEach _objLandebahnLichterLinksIds;
//{([0,0,0] nearestObject _x) hideObjectGlobal true;([0,0,0] nearestObject _x) enableSimulationGlobal false;} forEach _objControlTowerIds;
{([0,0,0] nearestObject _x) hideObjectGlobal true;([0,0,0] nearestObject _x) enableSimulationGlobal false;} forEach _objHousesRightTaxiwayIds;
{([0,0,0] nearestObject _x) hideObjectGlobal true;([0,0,0] nearestObject _x) enableSimulationGlobal false;} forEach _objTaxiwayStoerungenIds;
{([0,0,0] nearestObject _x) hideObjectGlobal true;([0,0,0] nearestObject _x) enableSimulationGlobal false;} forEach _objHangarIds;

hint format ["Zeit: %1",(diag_tickTime - _t1)];