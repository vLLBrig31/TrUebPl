#include "script_component.hpp"
SCRIPT(deployRopes);

if (GVAR(debug)) then systemChat format ["Deploy"];
  
CHECK(!([(vehicle player)] call FUNC(canSlingload)));

private ["_vehicle","_player","_ropeLength"];
_player = player;
_vehicle = vehicle player;
if(local _vehicle) then {
    _ropeLength = 5;
	private ["_existingRopes","_cargoRopes","_startLength","_slingloadpoint","_position"];
	_existingRopes = _vehicle getVariable [QGVAR(Ropes),[]];
	if(count _existingRopes == 0) then {
		_startLength = 0;
		if(vehicle _player == _player) then {
			_startLength = _ropeLength;
		};
		
		//isKindOf -> NH90 -> statt Slingload0 ist es slingload		
		_slingloadpoint = [];
		if ((_vehicle isKindOf "NH90") || (_vehicle isKindOf "EC635")) then {		
			_slingloadpoint = _vehicle selectionPosition ["slingload","Memory"];			
		};
		
		if (count _slingloadpoint == 0) then {
			_slingloadpoint = _vehicle selectionPosition ["slingload0","Memory"];;
		};	
		
		_cargoRopes = [];		
		_cargoRopes = _cargoRopes + [ropeCreate [_vehicle, _slingloadpoint, _startLength]]; 
		_cargoRopes = _cargoRopes + [ropeCreate [_vehicle, _slingloadpoint, _startLength]]; 
		_cargoRopes = _cargoRopes + [ropeCreate [_vehicle, _slingloadpoint, _startLength]]; 
		_cargoRopes = _cargoRopes + [ropeCreate [_vehicle, _slingloadpoint, _startLength]]; 
		_vehicle setVariable [QGVAR(Ropes),_cargoRopes,true];

		private ["_object","_myaction","_helper"];
		_object = _cargoRopes select 0;
		_position = position _object;
		_position = [_position select 0,_position select 1, (_position select 2) - 1];
		_helper = createVehicle["PortableHelipadLight_01_red_F",_position,[],0,"CAN_COLLIDE"];
		_helper setVariable [QGVAR(ParentVehicle),_vehicle,true];
		_position = position _helper;
		
		if (_position select 2 < 0) then {
			_helper setPos[_position select 0,_position select 1,0];
		};
		
		{
			[_helper, [0, 0, 0], [0,0,-1]] ropeAttachTo _x;
			ropeUnwind [_x, 5, _ropeLength];
		}  forEach (_vehicle getVariable [QGVAR(Ropes),[]]);		
		
		_myaction = [QGVAR(take_rope),(localize LSTRING(TAKEROPE)),GVAR(RopeUI),{[_this] call FUNC(pickupRope);},{!([player] call FUNC(isDraggingRope))},{},[],[0,0,0.2],30] call ace_interact_menu_fnc_createAction;
		[_helper,[],_myaction] remoteExecCall [QFUNC(addACEAction),0,false];
		
		_vehicle addEventHandler ["RopeBreak",{_this call FUNC(breakRope)}];
		
		[_vehicle] spawn FUNC(OpenHud);
	};
};