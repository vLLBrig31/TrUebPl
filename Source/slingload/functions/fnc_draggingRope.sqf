#include "script_component.hpp"
SCRIPT(draggingRope);

if (GVAR(debug)) then systemChat format ["DragRope Started: %1", player];

player setVariable [QGVAR(isDragging),true];

while {(player getVariable [QGVAR(isDragging),false]) && (alive player)} do {
	private ["_objectsNear"];
	_objectsNear = nearestObjects [player,["Car","Tank","Ship","ReammoBox_F"],10];
	{
		private["_actionsObject","_found","_action"];
		_found = false;
		_actionsObject = _x getVariable ["ace_interact_menu_actions", []];
		{
			_action = (_x select 0) select 0;
						
			if (_action == QGVAR(attach_rope)) then {
				_found =true;
			}
		}forEach (_actionsObject);
	
		if (!_found) then {
			
			_myaction = [QGVAR(attach_rope),(localize LSTRING(ATTACHROPE)),GVAR(RopeUIKnot),{ [GVAR(helper),_this] call FUNC(attachRope);},{[player] call FUNC(isDraggingRope)}] call ace_interact_menu_fnc_createAction;
			[_x,["ACE_MainActions"], _myaction] call FUNC(addACEAction);
		};
	} forEach (_objectsNear);
	
	
	sleep 2;
};