#include "script_component.hpp"
SCRIPT(pickupRope);

if (GVAR(debug)) then systemChat format ["PickUpRope: %1",_this];

private ["_object"];

_object = (_this select 0) select 0;

if (GVAR(debug)) then systemChat format ["PickUpRope Object: %1",_object];

GVAR(helper) = _object;
_object attachTo [player, [0, -0.04, 0.05], "righthand"];
[] spawn FUNC(draggingRope);