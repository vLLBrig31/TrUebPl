#include "script_component.hpp"
SCRIPT(addAddionalRopes);

if (GVAR(debug)) then systemChat format ["Add Ropes"];

params ["_aceAction"];
private ["_existingRopes","_cargoRopes","_startLength","_slingloadpoint","_position","_ropeLength","_vehicle"];

_vehicle = _aceAction select 0;

if (GVAR(debug)) then systemChat format ["Add Ropes to: %1",_vehicle];

_existingRopes = _vehicle getVariable [QGVAR(Ropes),[]];
if(count _existingRopes == 0) then {
	_slingloadpoint = getCenterOfMass _vehicle;
	_slingloadpoint = [_slingloadpoint select 0, _slingloadpoint select 1, (_slingloadpoint select 2)-0.2];
	_ropeLength = 6;
	_startLength = _ropeLength;
	
	if (GVAR(debug)) then systemChat format ["Ropes Pos: %1",_slingloadpoint];
	if (GVAR(debug)) then systemChat format ["Ropes Length: %1",_startLength];
	if (GVAR(debug)) then systemChat format ["CanAttach: %1",(ropeAttachEnabled _vehicle)];
	
	_cargoRopes = [];

	for "_i" from 1 to 4 do {		
		_rope = ropeCreate [_vehicle, _slingloadpoint, _startLength];
		if (!isNull _rope) then {
			_cargoRopes = _cargoRopes + [_rope];
		};
	};
		
	if (GVAR(debug)) then systemChat format ["Ropes: %1",_cargoRopes];
	
	if (count _cargoRopes > 0) then {
	
		if(!isNull _vehicle) then {
			[_vehicle,QGVAR(addAddional_rope)] remoteExecCall [QFUNC(removeACEAction),0,false];
		};

		_vehicle setVariable [QGVAR(Ropes),_cargoRopes,true];
		
		private ["_object","_myaction","_helper"];
		_object = _cargoRopes select 0;
		_position = position _object;
		_position = [_position select 0,_position select 1, (_position select 2) - 1];
		_helper = createVehicle["PortableHelipadLight_01_red_F",_position,[],0,"CAN_COLLIDE"];
		_helper setVariable [QGVAR(ParentVehicle),_vehicle,true];
		_position = position _helper;
		
		if (_position select 2 < 0) then {
			_helper setPos[_position select 0,_position select 1,0];
		};
		
		{
			[_helper, [0, 0, 0], [0,0,-1]] ropeAttachTo _x;
			ropeUnwind [_x, 5, _ropeLength];
		}  forEach (_vehicle getVariable [QGVAR(Ropes),[]]);		
		
		_myaction = [QGVAR(take_rope),(localize LSTRING(TAKEROPE)),GVAR(RopeUI),{[_this] call FUNC(pickupRope);},{!([player] call FUNC(isDraggingRope))},{},[],[0,0,0.2],30] call ace_interact_menu_fnc_createAction;
		[_helper,[],_myaction] remoteExecCall [QFUNC(addACEAction),0,false];
		_vehicle addEventHandler ["RopeBreak",{_this call FUNC(breakRope)}];
		
		[[_helper]] call FUNC(pickupRope);
	};
};