#include "script_component.hpp"
SCRIPT(cutRopes);

private ["_vehicle","_existingRopes"];
_vehicle = vehicle player;

if (GVAR(debug)) then systemChat format ["CutRopes: %1",_vehicle];

_existingRopes = _vehicle getVariable [QGVAR(Ropes),[]];
if(count _existingRopes > 0) then {
	{
		ropeDestroy _x;
	} forEach _existingRopes;

	_vehicle setVariable [QGVAR(Ropes),[],true];
};
