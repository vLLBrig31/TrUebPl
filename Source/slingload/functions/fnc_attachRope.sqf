#include "script_component.hpp"
SCRIPT(attachRope);

params ["_helper","_AceAction"];

_cargo = _AceAction select 0;
_vehicle = _helper getVariable [QGVAR(ParentVehicle),objNull];

if (GVAR(debug)) then systemChat format ["Attach from %1",_vehicle];
if (GVAR(debug)) then systemChat format ["Attach to %1",_cargo];

if(!isNull _vehicle) then {
	private ["_ropes","_attachmentPoints","_objDistance","_ropeLength","_i"];
	
	_ropes = _vehicle getVariable [QGVAR(Ropes),[]];
	if(count _ropes > 0) then {

		_ropeLength = (ropeLength (_ropes select 0));
		_objDistance = (_cargo distance _vehicle) + 2;
		if( _objDistance > _ropeLength ) then {
			//Hint
			hint "Seile zu kurz";
			if (GVAR(debug)) then systemChat format ["Rope too Short"];
			//[["The cargo ropes are too short. Move vehicle closer.", false],"ASL_Hint",_player] call ASL_RemoteExec;
		} else {
			//Get Attachmentpoints
			_attachmentPoints = [_cargo] call FUNC(getCargoAttachmentPoints);
			if (GVAR(debug)) then systemChat format ["AttachPoints: %1",_attachmentPoints];
			
			if (count _attachmentPoints > 0) then {		
				//Stop Dragging	
				player setVariable [QGVAR(isDragging),false];
				
				//delete Eventhandlers
				_vehicle removeAllEventHandlers "RopeBreak";
				
				//Detach Helper + Delete
				GVAR(helper) = objNull;
				detach _helper;
				{
					_helper ropeDetach _x;
				} forEach (_ropes);
				deleteVehicle _helper;			
				
				//Ropes to Cargo
				_i = 0;
				if (count _attachmentPoints < 4) then {
					[_cargo, _attachmentPoints select 0, [0,0,-1]] ropeAttachTo (_ropes select 0);
					[_cargo, _attachmentPoints select 0, [0,0,-1]] ropeAttachTo (_ropes select 1);
					[_cargo, _attachmentPoints select 0, [0,0,-1]] ropeAttachTo (_ropes select 2);
					[_cargo, _attachmentPoints select 0, [0,0,-1]] ropeAttachTo (_ropes select 3);
				} else {
					{
						[_cargo, _x, [0,0,-1]] ropeAttachTo (_ropes select _i);
						_i = _i + 1;
					} forEach _attachmentPoints;
				};
				
				_myaction = [QGVAR(detach_rope),(localize LSTRING(DETACHROPE)),GVAR(RopeUIRemove),{[_this] call FUNC(detachRope);},{!([player] call FUNC(isDraggingRope))}] call ace_interact_menu_fnc_createAction;
				[_cargo,["ACE_MainActions"],_myaction] remoteExecCall [QFUNC(addACEAction),0,false];
				
				if (_cargo isKindOf "AllVehicles") then {
					_myaction = [QGVAR(addAddional_rope),(localize LSTRING(EXTENDROPE)),GVAR(RopeUI),{[_this] call FUNC(addAddionalRopes);},{!([player] call FUNC(isDraggingRope))}] call ace_interact_menu_fnc_createAction;
					[_cargo,["ACE_MainActions"],_myaction] remoteExecCall [QFUNC(addACEAction),0,false];
				};
				
			} else	{
				if (GVAR(debug)) then systemChat format ["No AttachPoints found"];
				hint "Fehler bei Verbindungspunkten";
			};
		};
	};
};


