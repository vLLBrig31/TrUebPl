#include "script_component.hpp"
SCRIPT(extendRopes);

private ["_vehicle","_existingRopes"];
_vehicle = vehicle player;

if (GVAR(debug)) then systemChat format ["ExtendRopes: %1",_vehicle];

_existingRopes = _vehicle getVariable [QGVAR(Ropes),[]];
if(count _existingRopes > 0) then {
	_ropeLength = ropeLength (_existingRopes select 0);
	if(_ropeLength <= 50 ) then {
		{
			ropeUnwind [_x, 3, 5, true];
		} forEach _existingRopes;
	};
};