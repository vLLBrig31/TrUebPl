#include "script_component.hpp"
SCRIPT(breakRope);

params ["_object","_rope","_objectLost","_dragger"];

if (GVAR(debug)) then systemChat format ["BreakRope: %1",_rope];

if (!isNull attachedTo _objectLost) then {
	detach _objectLost;
	
	_dragger = attachedTo _objectLost;
	_dragger setVariable [QGVAR(isDragging),false,true];
	
	_posEndRope = ropeEndPosition _rope;

	_objectLost setPos [((_posEndRope select 0) select 0), ((_posEndRope select 0) select 1), 0];
	{
		[_objectLost, [0, 0, 0], [0,0,-1]] ropeAttachTo _x;
	}  forEach (_object getVariable [QGVAR(Ropes),[]]);
};