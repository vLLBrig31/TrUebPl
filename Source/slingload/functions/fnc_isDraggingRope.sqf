#include "script_component.hpp"
SCRIPT(isDraggingRope);

params ["_player"];

_isDragging = false;

if (!isNull _player) then {
	_isDragging = player getVariable [QGVAR(isDragging),false];	
};

_isDragging;