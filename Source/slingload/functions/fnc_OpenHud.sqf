#include "script_component.hpp"
SCRIPT(openHUD);

private["_dlg","_ctrl","_vehicle"];

_vehicle = _this select 0;
CHECK(!([_vehicle] call FUNC(canSlingload)));
CHECK(GVAR(showHUD));

([QGVAR(HUD)] call BIS_fnc_rscLayer) cutRsc [QGVAR(HUD), "PLAIN",0,true];

disableSerialization;
_dlg = uiNamespace getVariable [QGVAR(HUD), displayNull];
if (GVAR(debug)) then systemChat format ["HUD Dialog: %1",_dlg];

if (!(isNull _dlg)) then {
	GVAR(showHUD) = true;
	_ctrl = (_dlg displayCtrl 1002);
	
	while {GVAR(showHUD)} do {	
		private["_ropes"];
		_ropes = [];
		if (!((vehicle player) == _vehicle)) then {
			GVAR(showHUD) = false;
		};
		
		if (isNull _vehicle) then {
			GVAR(showHUD) = false;
		} else {
			_ropes = _vehicle getVariable [QGVAR(Ropes),[]];
			if (count _ropes == 0) then {GVAR(showHUD) = false;};
		};

		if GVAR(showHUD) then {		
			_ropeLength = (ropeLength (_ropes select 0));
			_ctrl ctrlSetText (format["%1 m",_ropeLength]);
			if (GVAR(debug)) then systemChat format ["HUD RopeLength: %1",(round _ropeLength)];
			if (GVAR(debug)) then systemChat format ["HUD Text: %1",ctrlText _ctrl];
			_ctrl ctrlCommit 0;
			sleep 5;
		};
		
	};
};

if (GVAR(debug)) then systemChat format ["Close Dialog: %1",_dlg];
([QGVAR(HUD)] call BIS_fnc_rscLayer) cutText ["","PLAIN",0,true];

