#include "script_component.hpp"
SCRIPT(detachRope);

params ["_aceAction"];

if (GVAR(debug)) then systemChat format ["Detach from %1",_aceAction];

_cargo = _aceAction select 0;

if(!isNull _cargo) then {
	[_cargo,QGVAR(detach_rope)] remoteExecCall [QFUNC(removeACEAction),0,false];
	[_cargo,QGVAR(addAddional_rope)] remoteExecCall [QFUNC(removeACEAction),0,false];
};

if (GVAR(debug)) then systemChat format ["Detach from %1",_cargo];

_vehicle = ropeAttachedTo _cargo;
if(!isNull _vehicle) then {
	_ropes = _vehicle getVariable [QGVAR(Ropes),[]];
	if(count _ropes > 0) then {
		{
			_cargo ropeDetach _x;
		} forEach (_ropes);
		
		private ["_object","_myaction","_helper","_position"];
		_object = _ropes select 0;
		_position = position _object;
		_position = [_position select 0,_position select 1, (_position select 2) - 1];
		_helper = createVehicle["PortableHelipadLight_01_red_F",_position,[],0,"CAN_COLLIDE"];
		_helper setVariable [QGVAR(ParentVehicle),_vehicle,true];
		_position = position _helper;
		
		if (_position select 2 < 0) then {
			_helper setPos[_position select 0,_position select 1,0];
		};
		
		{
			[_helper, [0, 0, 0], [0,0,-1]] ropeAttachTo _x;			
		}  forEach (_ropes);		
		
		_myaction = [QGVAR(take_rope),(localize LSTRING(TAKEROPE)),GVAR(RopeUI),{[_this] call FUNC(pickupRope);},{!([player] call FUNC(isDraggingRope))},{},[],[0,0,0.2],30] call ace_interact_menu_fnc_createAction;
		[_helper,[],_myaction] remoteExecCall [QFUNC(addACEActionMP),0,false];
				
		_vehicle addEventHandler ["RopeBreak",{_this call FUNC(breakRope)}];
		
		[[_helper]] call FUNC(pickupRope);
	};
};


