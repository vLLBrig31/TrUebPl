#include "script_component.hpp"
SCRIPT(canSlingload);

params ["_vehicle"];

private ["_isSupported"];
_isSupported = false;
if(not isNull _vehicle) then {
	if(_vehicle isKindOf "Helicopter") then {
		_isSupported = true;
	};
};
_isSupported;