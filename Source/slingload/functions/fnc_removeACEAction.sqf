#include "script_component.hpp"
SCRIPT(removeACEAction);

params["_object","_actionName"];
if (GVAR(debug)) then systemChat format ["REMOVE ACE Action Object: %1  Action: %2",_object,_actionName];

private["_actionsObject","_found","_action","_i","_currIndex"];
_actionsObject = _object getVariable ["ace_interact_menu_actions", []];

_currIndex = 0;
if (count _actionsObject > 0) then {
	for "_i" from 0 to (count _actionsObject) do {		
		_action = ((_actionsObject select _currIndex) select 0) select 0;
		if (_action == _actionName) then {
			_actionsObject deleteAt _currIndex;
			_currIndex = _currIndex - 1;
		};
		
		_currIndex = _currIndex + 1;
		if (_currIndex >= count _actionsObject) exitWith {};
	};
};
