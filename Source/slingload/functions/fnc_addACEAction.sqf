#include "script_component.hpp"
SCRIPT(addACEAction);

params["_object","_Category","_aceAction"];
if (GVAR(debug)) then systemChat format ["ADD ACE Action Object: %1  Action: %2",_object,_aceAction];
[_object, 0, _Category, _aceAction] call ace_interact_menu_fnc_addActionToObject;
