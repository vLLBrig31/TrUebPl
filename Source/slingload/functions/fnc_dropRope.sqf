#include "script_component.hpp"
SCRIPT(dropRope);
params ["_object"];

if (GVAR(debug)) then systemChat format ["DropRope: %1", _object];

if (!isNull attachedTo _object) then {
	detach _object;
	_object setPos[getPos _object select 0, getPos _object select 1, 0];
	player setVariable [QGVAR(isDragging),false];
};