#include "script_component.hpp"
#include "\a3\editor_f\Data\Scripts\dikCodes.h"

SCRIPT(init);


private["_root"];
_root = parsingNamespace getVariable "D_ROOTDIR";
GVAR(RopeUI) = _root+"slingload\ui\Rope.paa";
GVAR(RopeUIKnot) = _root+"slingload\ui\RopeKnot.paa";
GVAR(RopeUIRemove) = _root+"slingload\ui\RopeRemove.paa";

GVAR(debug) = true;
GVAR(showHUD) = false;

if (GVAR(debug)) then systemChat format ["Init-Slingload"];

//PilotCrew 
[(localize LSTRING(MODNAME)),QGVAR(KEY_deployRopes), (localize LSTRING(DEPLOYROPES)), {call FUNC(deployRopes)}, {}, [DIK_5, [false, false, false]]] call CBA_fnc_addKeybind;
[(localize LSTRING(MODNAME)),QGVAR(KEY_cutRopes), (localize LSTRING(CUTROPES)), {call FUNC(cutRopes)}, {}, [DIK_0, [false, false, false]]] call CBA_fnc_addKeybind;
[(localize LSTRING(MODNAME)),QGVAR(KEY_extendRopes), (localize LSTRING(EXTENDROPES)), {call FUNC(extendRopes)}, {}, [DIK_6, [false, false, false]]] call CBA_fnc_addKeybind;
[(localize LSTRING(MODNAME)),QGVAR(KEY_shortenRopes), (localize LSTRING(SHORTENROPES)), {call FUNC(shortenRopes)}, {}, [DIK_7, [false, false, false]]] call CBA_fnc_addKeybind;
[(localize LSTRING(MODNAME)),QGVAR(KEY_showHud), (localize LSTRING(SHOW_HUD)), {[vehicle player] spawn FUNC(openHUD)}, {}, [DIK_9, [false, false, false]]] call CBA_fnc_addKeybind;


//ACE-Actions
_myaction = [QGVAR(drop_rope),(localize LSTRING(DROPROPE)),GVAR(RopeUI),{[GVAR(helper)] call FUNC(dropRope);},{[player] call FUNC(isDraggingRope)}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions"], _myaction] call ace_interact_menu_fnc_addActionToObject;