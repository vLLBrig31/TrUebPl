#include "script_component.hpp"

PREP(init);
PREP(addACEAction);
PREP(removeACEAction);

PREP(canSlingload);
PREP(breakRope);

PREP(pickupRope);
PREP(dropRope);
PREP(draggingRope);
PREP(isDraggingRope);
PREP(attachRope);
PREP(detachRope);
PREP(addAddionalRopes);

PREP(deployRopes);
PREP(cutRopes);
PREP(extendRopes);
PREP(shortenRopes);

PREP(getCargoAttachmentPoints);

PREP(OpenHud);
