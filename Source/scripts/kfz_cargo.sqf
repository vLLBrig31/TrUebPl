private ["_veh"];

if (!isServer) exitWith {};

_veh = _this select 0;
clearWeaponCargoGlobal _veh; 
clearMagazineCargoGlobal _veh; 
clearItemCargoGlobal _veh;
_veh addMagazineCargoGlobal ["SatchelCharge_Remote_Mag",1];
_veh addItemCargoGlobal ["Toolkit",1];
_veh addWeaponCargoGlobal ["BWA3_G36",2];
_veh addMagazineCargoGlobal ["BWA3_Pzf3_IT",1];
_veh addWeaponCargoGlobal ["BWA3_Pzf3",1];
_veh addMagazineCargoGlobal ["BWA3_30Rnd_556x45_G36_AP",10];
_veh addMagazineCargoGlobal ["BWA3_30Rnd_556x45_G36_Tracer",10];
_veh addMagazineCargoGlobal ["SmokeShell",5];						
_veh addMagazineCargoGlobal ["SmokeShellOrange",2];
_veh addMagazineCargoGlobal ["UGL_FlareWhite_F",1];
_veh addMagazineCargoGlobal ["UGL_FlareGreen_F",1];
_veh addMagazineCargoGlobal ["UGL_FlareYellow_F",1];
_veh addMagazineCargoGlobal ["UGL_FlareRed_F",1];
_veh addMagazineCargoGlobal ["HandGrenade",2];
_veh addItemCargoGlobal ["BWA3_acc_LLM01_irlaser",2];
_veh addWeaponCargoGlobal ["BWA3_Vector",1];
