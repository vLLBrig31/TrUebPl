//Cargoscript für Nato Supply Box
private ["_veh"];

if (!isServer) exitWith {};

_veh = _this select 0;
clearWeaponCargoGlobal _veh; 
clearMagazineCargoGlobal _veh; 
clearItemCargoGlobal _veh;
_veh addItemCargoGlobal ["BWA3_Pzf3_Loaded", 1]; 
_veh addItemCargoGlobal ["BWA3_30Rnd_556x45_G36", 30]; 
_veh addItemCargoGlobal ["BWA3_120Rnd_762x51", 10]; 
_veh addItemCargoGlobal ["BWA3_1200Rnd_762x51", 1]; 
_veh addItemCargoGlobal ["BWA3_20Rnd_762x51_G28", 12]; 
_veh addItemCargoGlobal ["BWA3_Pzf3_IT", 1]; 
_veh addItemCargoGlobal ["SmokeShellGreen", 4]; 
_veh addItemCargoGlobal ["SmokeShellRed", 4]; 
_veh addItemCargoGlobal ["BWA3_DM51A1", 8]; 
_veh addItemCargoGlobal ["BWA3_DM25", 4];
_veh addItemCargoGlobal ["ACE_SpareBarrel", 3]; 
_veh addItemCargoGlobal ["V_Rangemaster_belt", 1]; 
_veh addItemCargoGlobal ["ACE_fieldDressing", 40]; 
_veh addItemCargoGlobal ["ACE_packingBandage", 40]; 
_veh addItemCargoGlobal ["ACE_elasticBandage", 20]; 
_veh addItemCargoGlobal ["ACE_tourniquet", 10];
_veh addItemCargoGlobal ["ACE_quikclot", 10];
_veh addItemCargoGlobal ["B_AssaultPack_cbr", 10];
