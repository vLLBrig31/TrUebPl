private ["_veh"];

if (!isServer) exitWith {};

_veh = _this select 0;
clearWeaponCargoGlobal _veh; 
clearMagazineCargoGlobal _veh; 
clearItemCargoGlobal _veh;
_veh addWeaponCargoGlobal ["BWA3_G36",2];
_veh addWeaponCargoGlobal ["BWA3_G82",2];
_veh addBackpackCargoGlobal ["ffaa_Tripod_Bag",2];
_veh addBackpackCargoGlobal ["ffaa_milan_tripode_Bag",2];
_veh addMagazineCargoGlobal ["BWA3_Pzf3_IT",2];
_veh addWeaponCargoGlobal ["BWA3_Pzf3",2];

_veh addItemCargoGlobal ["BWA3_acc_LLM01_irlaser",2];

_veh addItemCargoGlobal ["BWA3_Vector",2];
_veh addItemCargoGlobal ["Rangefinder",2];
_veh addItemCargoGlobal ["B_UavTerminal",2];
_veh addItemCargoGlobal ["BWA3_ItemKestrel",2];
_veh addItemCargoGlobal ["NVGoggles",2];

_veh addMagazineCargoGlobal ["BWA3_DM25",5];
_veh addMagazineCargoGlobal ["SmokeShellBlue",2];
_veh addMagazineCargoGlobal ["BWA3_DM32_Orange",2];
_veh addBackpackCargoGlobal ["B_MAV_B_BACKPACK",2];

_veh addMagazineCargoGlobal ["BWA3_10Rnd_127x99_G82_Raufoss_Tracer_Dim",10];
_veh addMagazineCargoGlobal ["BWA3_10Rnd_127x99_G82_AP",15];
_veh addMagazineCargoGlobal ["BWA3_10Rnd_762x51_G28_LR",10];
_veh addMagazineCargoGlobal ["BWA3_10Rnd_762x51_G28_Tracer",10];
_veh addMagazineCargoGlobal ["BWA3_30Rnd_556x45_G36_AP",10];
_veh addMagazineCargoGlobal ["BWA3_30Rnd_556x45_G36_Tracer",10];

_veh addMagazineCargoGlobal ["SatchelCharge_Remote_Mag",1];
_veh addMagazineCargoGlobal ["DemoCharge_Remote_Mag",5];
_veh addMagazineCargoGlobal ["ClaymoreDirectionalMine_Remote_Mag",4];

_veh addItemCargoGlobal ["BWA3_Uniform_Ghillie_Fleck",2];