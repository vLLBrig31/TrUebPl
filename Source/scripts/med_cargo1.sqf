//Cargoscript für UH60M-MEV
private ["_veh"];

if (!isServer) exitWith {};

_veh = _this select 0;
clearWeaponCargoGlobal _veh; 
clearMagazineCargoGlobal _veh; 
clearItemCargoGlobal _veh;
_veh addItemCargoGlobal ["ACE_fieldDressing",200];
_veh addItemCargoGlobal ["ACE_packingBandage",200];
_veh addItemCargoGlobal ["ACE_elasticBandage",150];
_veh addItemCargoGlobal ["ACE_tourniquet",20];
_veh addItemCargoGlobal ["ACE_morphine",30];
_veh addItemCargoGlobal ["ACE_atropine",30];
_veh addItemCargoGlobal ["ACE_epinephrine",30];
_veh addItemCargoGlobal ["ACE_salineIV",20];
_veh addItemCargoGlobal ["ACE_salineIV_500",45];
_veh addItemCargoGlobal ["ACE_quikclot",80];
_veh addItemCargoGlobal ["ACE_personalAidKit",20];
_veh addItemCargoGlobal ["ACE_surgicalKit",10];

