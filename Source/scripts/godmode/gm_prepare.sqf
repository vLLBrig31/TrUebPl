_u = _this select 1; 			//Unit: User
_stat = (_this select 3) select 0;	//Number: 1 = on | 0 = off

if (_stat == 1) then {
	_u allowDamage false;
	_u globalChat format ["%1 wurde zu CHUCK NORRIS.",_u];
	GM_ON = _u;
	publicVariable "GM_ON";
};
if (_stat == 0) then {
	_u allowDamage true;
	_u globalChat format ["Das Leben von %1 ist vergänglich.",_u];
	GM_OFF = _u;
	publicVariable "GM_OFF";
};
