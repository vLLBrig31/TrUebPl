//Cargoscript für Taru - Medic POD
private ["_veh"];

if (!isServer) exitWith {};

_veh = _this select 0;
clearWeaponCargoGlobal _veh; 
clearMagazineCargoGlobal _veh; 
clearItemCargoGlobal _veh;
_veh addItemCargoGlobal ["ACE_fieldDressing", 80]; 
_veh addItemCargoGlobal ["ACE_packingBandage", 70]; 
_veh addItemCargoGlobal ["ACE_tourniquet", 4]; 
_veh addItemCargoGlobal ["ACE_elasticBandage", 20]; 
_veh addItemCargoGlobal ["ACE_morphine", 30]; 
_veh addItemCargoGlobal ["ACE_epinephrine", 30]; 
_veh addItemCargoGlobal ["ACE_atropine", 30]; 
_veh addItemCargoGlobal ["ACE_salineIV", 12]; 
_veh addItemCargoGlobal ["ACE_salineIV_500", 24]; 
_veh addItemCargoGlobal ["ACE_quikclot", 20];

