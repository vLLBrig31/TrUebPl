//Cargoscript für Taru - Cargo POD
private ["_veh"];

if (!isServer) exitWith {};

_veh = _this select 0;
clearWeaponCargoGlobal _veh; 
clearMagazineCargoGlobal _veh; 
clearItemCargoGlobal _veh;
_veh addItemCargoGlobal ["BWA3_30Rnd_556x45_G36", 180]; 
_veh addItemCargoGlobal ["BWA3_20Rnd_762x51_G28", 70]; 
_veh addItemCargoGlobal ["BWA3_120Rnd_762x51", 35]; 
_veh addItemCargoGlobal ["BWA3_Pzf3", 5]; 
_veh addItemCargoGlobal ["BWA3_Pzf3_IT", 5]; 
_veh addItemCargoGlobal ["BWA3_15Rnd_9x19_P8", 15]; 
_veh addItemCargoGlobal ["BWA3_40Rnd_46x30_MP7", 20]; 
_veh addItemCargoGlobal ["BWA3_30Rnd_556x45_G36_AP", 60]; 
_veh addItemCargoGlobal ["tf47_m3maaws", 2]; 
_veh addItemCargoGlobal ["tf47_m3maaws_HEAT", 12]; 
_veh addItemCargoGlobal ["tf47_m3maaws_HEDP", 8]; 
_veh addItemCargoGlobal ["tf47_m3maaws_HE", 8]; 
_veh addItemCargoGlobal ["tf47_m3maaws_SMOKE", 8]; 
_veh addItemCargoGlobal ["tf47_m3maaws_ILLUM", 8]; 
_veh addItemCargoGlobal ["BWA3_Fliegerfaust", 2]; 
_veh addItemCargoGlobal ["BWA3_RGW90", 4]; 
_veh addItemCargoGlobal ["BWA3_RGW90_HH", 4]; 
_veh addItemCargoGlobal ["BWA3_RGW90_HH", 4]; 
_veh addItemCargoGlobal ["1Rnd_HE_Grenade_shell", 30];  
_veh addItemCargoGlobal ["1Rnd_Smoke_Grenade_shell", 10]; 
_veh addItemCargoGlobal ["1Rnd_SmokeRed_Grenade_shell", 10]; 
_veh addItemCargoGlobal ["1Rnd_SmokeGreen_Grenade_shell", 10];
_veh addItemCargoGlobal ["UGL_FlareWhite_F", 10]; 
_veh addItemCargoGlobal ["UGL_FlareGreen_F", 10]; 
_veh addItemCargoGlobal ["UGL_FlareRed_F", 10]; 
_veh addItemCargoGlobal ["UGL_FlareCIR_F", 10]; 
_veh addItemCargoGlobal ["BWA3_DM51A1", 13]; 
_veh addItemCargoGlobal ["BWA3_DM25", 10]; 
_veh addItemCargoGlobal ["rhs_mag_m18_green", 10]; 
_veh addItemCargoGlobal ["rhs_mag_m18_red", 10]; 
_veh addItemCargoGlobal ["BWA3_Fliegerfaust_Mag", 4]; 
_veh addItemCargoGlobal ["ACE_packingBandage", 30]; 
_veh addItemCargoGlobal ["ACE_packingBandage", 15];

