//Cargoscript für UH60M
private ["_veh"];

if (!isServer) exitWith {};

_veh = _this select 0;
clearWeaponCargoGlobal _veh; 
clearMagazineCargoGlobal _veh; 
clearItemCargoGlobal _veh;
_veh addItemCargoGlobal ["ACE_fieldDressing",200];
_veh addItemCargoGlobal ["ACE_packingBandage",200];
_veh addItemCargoGlobal ["ACE_elasticBandage",150];
_veh addItemCargoGlobal ["ACE_tourniquet",20];
_veh addItemCargoGlobal ["ACE_morphine",30];
_veh addItemCargoGlobal ["ACE_atropine",30];
_veh addItemCargoGlobal ["ACE_epinephrine",30];
_veh addItemCargoGlobal ["ACE_salineIV_500",45];
_veh addItemCargoGlobal ["ACE_quikclot",80];
_veh addItemCargoGlobal ["ACE_personalAidKit",20];
_veh addMagazineCargoGlobal ["BWA3_30Rnd_556x45_G36",40];
_veh addMagazineCargoGlobal ["BWA3_20Rnd_762x51_G28",20];
_veh addMagazineCargoGlobal ["BWA3_120Rnd_762x51",7];

