#include "script_component.hpp"
SCRIPT(init);

enableSaving [false,false];
enableRadio false;

// Godmode Messages
"GM_ON" addPublicVariableEventHandler {(_this select 1) allowDamage false; (_this select 1) globalChat format ["%1 wurde zu CHUCK NORRIS.",(_this select 1)]};
"GM_OFF" addPublicVariableEventHandler {(_this select 1) allowDamage true; (_this select 1) globalChat format ["Das Leben von %1 ist vergänglich.",(_this select 1)]};

//CoPilot
call compile preprocessFileLineNumbers "vllBrig31_CoPilot\dsp_copilot.sqf";
call dsp_fnc_Init; 

_handle = execVM "vllBrig31_runwayExtensions\RunwayExtensions.sqf"; // Runway Lights + Additions
_handle = execVM "vllBrig31_Jump\JumpLocation_Init.sqf"; // Jump

// Werthles Headless Script Parameters v2.3
// 1. Repeating - true/Once - false,
// 2. Time between repeats (seconds),
// 3. Debug available for all - true/Just available for admin/host - false,
// 4. Advanced balancing - true/Simple balancing - false,
// 5. Delay before executing (seconds),
// 6. Additional syncing time between groups transferred to try to reduce bad unit transfer caused by desyncs (seconds)
// 7. Display an initial setup report after the first cycle, showing the number of units moved to HCs,
// 8. Addition phrases to look for when checking whether to ignore.
// Unit names, group names, unit's current transport vehicle, modules synced to units and unit class names will all be checked for these phrases
// Format:
// ["UnitName","GroupCallsignName","SupportProviderModule1","TypeOfUnit"]
// E.g. ["BLUE1","AlphaSquad","B_Heli_Transport_01_camo_F"] (including ""s)
// Specifying "B_Heli" would stop all units with that class type from transferring to HCs
// However, if you specify "BLUE1", "NAVYBLUE10" will also be ignored

[] execVM "VCOM_Driving\init.sqf";
[] execVM "VCOMAI\init.sqf";

[true,30,false,false,30,3,true,[]] execVM "WerthlesHeadless.sqf";

