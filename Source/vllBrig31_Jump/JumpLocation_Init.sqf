if (!isServer) exitWith {};

vllbrig31_Jump_createLocation = compile preprocessFile "vllBrig31_Jump\createJumpLocation.sqf";
vllbrig31_Jump_Spawn = [4194.3,10855.6,339];
vllbrig31_Jump_Location1 = [574,11295];
vllbrig31_Jump_Location2 = [526,11400];
vllbrig31_Jump_C130_300 = [vllbrig31_Jump_Location1,[300]] call vllbrig31_Jump_createLocation;
vllbrig31_Jump_C130_500 = [vllbrig31_Jump_Location2,[500]] call vllbrig31_Jump_createLocation;

publicVariable "vllbrig31_Jump_C130_300";
publicVariable "vllbrig31_Jump_C130_500";

_Position = [597,11409.3,0];

//_Position = [(Position select 0) - 2,(Position select 1) - 2,Position select 2];

_jumpmasterMain = "ARC_GER_Soldier_SL" createVehicle (_Position);
_jumpmasterMain setIdentity "JumpMasterMain";
_jumpmasterMain allowDamage false; 
removeAllWeapons _jumpmasterMain; 
removeAllItems _jumpmasterMain; 
removeHeadgear _jumpmasterMain; 
removeAllAssignedItems _jumpmasterMain;
removeVest _jumpmasterMain; 
_jumpmasterMain addHeadgear "GER_Beret_Fallschirmjaeger";
sleep 1;
_jumpmasterMain setVectorUp [0,0,1]; 
_jumpmasterMain setDir 50;
sleep 1;
_jumpmasterMain switchMove "AmovPercMstpSnonWnonDnon_Ease";

[[_jumpmasterMain, [("<t color='#006699'>" + ("Ausrüstung") + "</t>"), "['Open',true] call BIS_fnc_arsenal;"]], "addAction", true, true] call BIS_fnc_MP;
[[_jumpmasterMain, [("<t color='#13dc18'>" + ("Absprung Höhe 300m") + "</t>"), "execVM 'vllBrig31_Jump\teleportToJumpLocation1.sqf';"]], "addAction", true, true] call BIS_fnc_MP;
[[_jumpmasterMain, [("<t color='#dc4213'>" + ("Absprung Höhe 500m") + "</t>"), "execVM 'vllBrig31_Jump\teleportToJumpLocation2.sqf';"]], "addAction", true, true] call BIS_fnc_MP;


