private ["_jumpLoc","_jumpHeight"];
_jumpLoc = _this select 0;
_jumpHeight = _this select 1;


private ["_jumpPos","_helper","_JumpC130","_jumpmaster"];
_jumpPos = _jumpLoc + _jumpHeight;

_helper = "Sign_Sphere25cm_F" createVehicle (vllbrig31_Jump_Spawn);
hideObjectGlobal _helper; _helper setVectorUp [0,0,1]; _helper setDir 210;
sleep 1;
_helper setPos _jumpPos;

_JumpC130 = "RHS_C130J" createVehicle (vllbrig31_Jump_Spawn);
_JumpC130 allowDamage false; 
_JumpC130 setVectorUp [0,0,1];
sleep 1;
_JumpC130 attachTo [_helper,[0,0,0]];
_JumpC130 setVectorUp [0.2,0.3,1];

_JumpC130 engineOn true;
_JumpC130 animateDoor ['ramp',0.9];
//_JumpC130 animateDoor ['door_2_1',1];
//_JumpC130 animateDoor ['door_2_2',1];

_lightC130 = "chemlight_blue" createVehicle (position _helper);  
_lightC130 attachTo [_JumpC130,[0,4,0.7]];

_JumpC130