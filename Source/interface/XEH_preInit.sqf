#include "script_component.hpp"

PREP(disp_blur);
PREP(disp_info);
PREP(disp_info_global);
PREP(disp_message);
PREP(disp_message_global);
PREP(disp_progressbar);
PREP(disp_timer);

PREPS(crate,add);
PREPS(crate,addAction);
PREPS(crate,createlists);
PREPS(crate,draw3D);
PREPS(crate,filter);
PREPS(crate,getMod);
PREPS(crate,mass);
PREPS(crate,OnLoad);
PREPS(crate,OpenMenu);
PREPS(crate,save);
PREPS(crate,spawn);

PREPS(save,delete);
PREPS(save,load);
PREPS(save,OnOpen);
PREPS(save,OpenMenu);
PREPS(save,save);
PREPS(save,select);
