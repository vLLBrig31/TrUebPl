// Script by Dr.Thodt
#include "script_component.hpp"
SCRIPT(init);
if (isDedicated) exitWith {};
private ["_name","_vehicle","_vehname","_hudnames","_ui","_injured","_symbol","_isBleeding","_hasLostBlood","_hasPain","_color"];   
disableSerialization;
while {true} do  {
	([QUOTE(EGVAR(interface,HudNames))] call BIS_fnc_rscLayer) cutRsc [QUOTE(EGVAR(interface,HudNames)),"PLAIN"];
	_ui = uiNameSpace getVariable QUOTE(EGVAR(interface,HudNames));
	_HudNames = _ui displayCtrl 700201;
	if((player != vehicle player) && !(assignedVehicle player isKindOf "UGV_01_base_F") && !(assignedVehicle player isKindOf "ParachuteBase") && !(assignedVehicle player isKindOf "StaticWeapon")) then
	{		
		_name = "";
		_vehicle = assignedVehicle player;
		_vehname= getText (configFile >> "CfgVehicles" >> (typeOf vehicle player) >> "DisplayName");
		_name = format ["<t size='1.25' color='#556b2f'>%1</t><br/>", _vehname];

		{ // Crew forEach Schleife
			_injured = false;
			_hasLostBlood = (_x getVariable "ACE_medical_hasLostBlood");
			_hasPain = (_x getVariable "ACE_medical_hasPain");
			_isBleeding = (_x getVariable "ACE_medical_isBleeding");
			
			if ((_x getHitPointDamage "HitHead" > 0) || (_x getHitPointDamage "HitBody" > 0) || (_x getHitPointDamage "HitLeftArm" > 0) || (_x getHitPointDamage "HitRightArm" > 0) || (_x getHitPointDamage "HitLeftLeg" > 0) || (_x getHitPointDamage "HitRightLeg" > 0) || (_hasLostBlood > 0) || (_hasPain)) then {
				_injured = true;
			}
			else {			
				if (_isBleeding) then {
					_injured = true;
				};
			};
			
			_symbol = "a3\ui_f\data\IGUI\Cfg\Actions\getincargo_ca.paa";		
			if (commander _vehicle == _x) then {_symbol = "a3\ui_f\data\IGUI\Cfg\Actions\getincommander_ca.paa";};
			if (driver _vehicle == _x) then {_symbol = "a3\ui_f\data\IGUI\Cfg\Actions\getindriver_ca.paa";};
			if (gunner _vehicle == _x) then {_symbol = "a3\ui_f\data\IGUI\Cfg\Actions\getingunner_ca.paa";};
			
			_color = "#6b8e23"; //grün
			if (_injured) then { _color = "#ffa200"; }; //gelb
			if (_isBleeding) then { _color = "#cd21cf"; }; //lila
			if (_x getVariable "ACE_isUnconscious") then { _color = "#8e2323"; }; //rot
			
			_name = format ["<t size='0.85' color='%4'>%1<img size='0.7' color='%4' image='%3'/> %2<br/></t>", _name, (name _x),_symbol,_color];
		} forEach crew _vehicle;

		_HudNames ctrlSetStructuredText parseText _name;
		_HudNames ctrlCommit 0;
		};
	sleep 1;
  };