/*
	author: iJesuz, Dorbedo

	description:
		part of only pilots

	parameter:
		0: GROUP - group of player
		1: ARRAY - position of player
		2: UNIT - player
		3: ARRAY - turret
*/
#include "script_component.hpp"
SCRIPT(XEH_getIn);

params["_group","_position","_unit","_turret"];

CHECK(!isPlayer _unit)
